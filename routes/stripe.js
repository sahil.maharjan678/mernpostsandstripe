const express = require('express');
const router = express.Router();
const path = require('path');
const stripe = require('stripe')('sk_test_ynmPZbPVxr5nx3HCPFijisAl00tv0bxB4n');


router.post('/customer/create', (req, res) => {
    const token = req.body.token;
    if(!token) {
        return res.send({
            success: false,
            message: 'No Token'
        });
    }

    stripe.customers.create({
        source: token,
      })
        .then((customer) => {
            res.send({
                success: true,
                customer: customer,
                customerId: customer.id
            })
        })
        .catch(error => console.error(error));
});


router.post('/customer/subscribe', (req, res) => {
    let { plan, coupon } = req.body;
    plan = plan.toLowerCase().trim();
    coupon = coupon.toLowerCase().trim();

    const plans = ['bronze', 'silver', 'gold'];
    if(plans.indexOf(plan) === -1) {
        return res.send({
            success: false,
            message: 'Invalid plan'
        })
    }

    const customerId = 'cus_HFqNI7s9gMjQkW';

    const subscription = stripe.subscriptions.create({
        customer: customerId,
        items: [{plan: plan}],
        coupon: coupon
    });

    res.send({
        success: true,
        message: 'Done'
    })
})

router.use(function(req, res) {
	res.sendFile(path.join(__dirname, '../client/build/index.html'));
});

module.exports = router;