const express = require('express');
const router = express.Router();
const path = require('path');
const Post = require('../models/Post');


router.get('/', async (req, res) => {
    try{
        const posts = await Post.find();
        res.send(posts);
    }catch(error){
        res.status(500).send({ message: error });
    }
});

router.post('/', async (req, res) => {
    const post = new Post({
        title: req.body.title,
        description: req.body.description,
        type: req.body.type,
        url: req.body.url
    });
    try{
        const savedPost = await post.save();
        res.status(200).json(savedPost);
    }catch(err){
        res.status(500).send({message: err});
    }
});

router.get('/:postId', async (req, res) => {
    try{
        const post = await Post.findById(req.params.postId);
        res.json(post);
    }catch (error) {
        res.json({message: error});
    }
});

router.delete('/:postId', async (req, res) => {
    try{
        const removedPost = await Post.remove({ _id: req.params.postId });
        res.json(removedPost);
    }catch(error){
        res.send({message: error})
    }
});

router.patch('/:postId', async (req, res) => {
    try{
        const updatedPost = await Post.updateOne(
            { _id: req.params.postId }, 
            {$set: {title: req.body.title}}
            );
        res.json(updatedPost);
    }catch(error){
        res.send({message: error})
    }
});

router.use(function(req, res) {
	res.sendFile(path.join(__dirname, '../client/build/index.html'));
});

module.exports = router;