import React from 'react';
import {injectStripe} from 'react-stripe-elements';
import CardSection from './CardSection';
import axios from 'axios';

class CheckoutForm extends React.Component {
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit (ev){
        ev.preventDefault();
        this.props.stripe.createToken({}).then(({token}) => {
            console.log("Received Stripe Token:", token.id);
            axios({
              method: 'POST',
              url: '/stripe/customer/create',
              headers: {
                'Content-Type': 'application/json'
              },
              data: {
                token: token.id
              }
            })
            .then((response) => {
              console.log('response', response);
            }).catch((err) => {
              console.log('err', err);
            })

        });
    };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <CardSection />
        <button>Confirm order</button>
      </form>
    );
  }
}

export default injectStripe(CheckoutForm);