import React from 'react';
import {StripeProvider} from 'react-stripe-elements';
import MyStoreCheckout from './MyStoreCheckout';

export default function Stripe() {

    return(
        <div className="container">
            <h1 style={{margin: "10px 0"}}>Connect to Stripe</h1>
            <StripeProvider apiKey="pk_test_vxETrJqmHXkCMTRN5RXF68Go00gaOw9Btw">
                <MyStoreCheckout />
            </StripeProvider>
            <br/>
            <p>Note: Use 4242 4242 4242 4242 as Test Credit Card. This page creates a user in stripe and receives token from stripe and can be seen in our console.</p>
        </div>
    )
}