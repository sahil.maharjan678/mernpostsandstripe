import React, {Component} from 'react';
import { Button, Input } from 'reactstrap';
import axios from 'axios';


class StripePage2 extends Component {
    constructor(props){
        super(props);

        this.state = {
            coupon: '',
            currentPlan: 'bronze',
        }
        this.onCouponChange = this.onCouponChange.bind(this);
        this.switchPlan = this.switchPlan.bind(this);
        this.nextStep = this.nextStep.bind(this);
    }

    onCouponChange(event){
        this.setState({
            coupon: event.target.value
        })
    }

    switchPlan(currentPlan){
        this.setState({
            currentPlan
        });
    }

    nextStep() {
        const {coupon, currentPlan} = this.state;
        axios({
            method: 'POST',
            url: '/stripe/customer/subscribe',
            headers: {
              'Content-Type': 'application/json'
            },
            data: {
              plan: currentPlan,
              coupon: coupon
            }
          })
          .then((response) => {
            console.log('response', response);
          }).catch((err) => {
            console.log('err', err);
          })
    }

    render() {
        const {coupon, currentPlan} = this.state;

        console.log('coupon', coupon);
        console.log('currentPlan', currentPlan);

        const plans = ['bronze', 'silver', 'gold'];

        return (
           <div className="container">
               <h5 style={{margin: "10px 0"}}>Coupon</h5>
                <Input
                    type="text"
                    placeholder = "Coupon"
                    value={coupon}
                    onChange={this.onCouponChange}
                />
                <h5>Plans</h5>
                {
                    plans.map((plan) => {
                            return(
                                <Button 
                                    color={plan === currentPlan ? 'primary': 'info'}
                                    onClick={() => this.switchPlan(plan)}
                                >
                                    {plan}
                                </Button>
                            )
                    })
                }
                <br/><br/>
                <div>
                    <Button onClick={() => this.nextStep()}>Finish Order</Button>
                </div>
                <br/>
                <p>Note: This page confirms the product bought from stripe and uses the token from previous page and subscribes to monthly package plan of gold, silver or bronze and uses discount code if it is present in stripe.</p>
           </div>
        )
    }
}

export default StripePage2;