import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { CardColumns, Card, CardText, Badge, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

export default function Post() {
    const [postsData, setPostsData] = useState([]);

    useEffect(() => {
        axios.get('/posts')
            .then((res) => {
                setPostsData(res["data"]);
            }).catch((err) => {
                console.log(err);
            });
    }, []); 
    
    const calculateTime = (date) => {
        const dateObj = new Date(date)
        const day = dateObj.getUTCDate();
        const monthNames = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                        ];
        const month = monthNames[dateObj.getUTCMonth()];
        return `${month} ${day}`;
    };
    
    return(
        <div className="container">
            {postsData.length !== 0 && (
                <>
                    <h1 style={{margin: "10px 0"}}>All Posts</h1>
                    <CardColumns>
                        { postsData.map((data) => (
                            <Card body outline color="secondary" key={data.title}>
                                <Badge color={data.type === "text" ? "primary" : "success"}>{data.type.toUpperCase()}</Badge>
                                <h5 className="text-capitalize font-weight-bold">{data.title}</h5>
                                <p className="text-weight-light">{calculateTime(data.date)}</p>
                                <CardText style={{height: "125px", overflow: "hidden"}}>{data.description}</CardText>
                                <Link to={`/post/${data._id}`}>
                                    <Button>View Post</Button>
                                </Link>
                            </Card>    
                        ))
                        }
                    </CardColumns>
                </>
            )}
            {postsData.length === 0 && (
                <p style={{margin: "10px 0"}} className="lead">No Post Are Present, Create some Posts.</p>
            )}
        </div>
    )
}