import React, { useState } from 'react';
import { Form, FormGroup, Label, Input, Button} from 'reactstrap';
import axios from 'axios';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

const initialValues = {
    title: '',
    description: '',
    type: 'text',
    url: ''
}

export default function Post() {
    const [formData, setFormData] = useState(initialValues);

    function handleInputChange(event) {
        event.persist();
        setFormData((inputData) => ({ ...inputData, [event.target.name]: event.target.value }))
    };

    function handleFormDataSubmit(){
        const url = '/posts';
        axios({
            method: 'POST',
            url,
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            data: formData
        }).then((res) => {
            history.go(`/post/${res.data._id}`);
        }).catch((err) => {
            console.log("erroorr",err);
            alert("There was an error");
        });
    }


    return(
        <div className="container">
            <Form style={{margin: "10px 0"}}>
                <FormGroup>
                    <Label for="title">Title</Label>
                    <Input 
                        type="text" 
                        name="title" 
                        id="title" 
                        placeholder="Enter Title of the Post" 
                        onChange={(e) => {handleInputChange(e)}}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="description">Description</Label>
                    <Input 
                        type="textarea" 
                        name="description" 
                        id="description" 
                        placeholder="Give a brief description of this Post" 
                        onChange={(e) => {handleInputChange(e)}}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="type">Type</Label>
                    <Input type="select" name="type" id="type" onChange={(e) => {handleInputChange(e)}}>
                        <option defaultValue value="text">Text</option>
                        <option value="video">Video</option>
                    </Input>
                </FormGroup>
                {
                    (formData.type === "video") && (
                        <FormGroup>
                            <Label for="url">URL</Label>
                            <Input 
                                type="url" 
                                name="url" 
                                id="url" 
                                placeholder="Vimeo OR Youtube URL" 
                                onChange={(e) => {handleInputChange(e)}}
                            />
                        </FormGroup>
                    )
                }
                <Button onClick={handleFormDataSubmit}>Publish</Button>
            </Form>
        </div>
    )
}