import React,{ useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { Jumbotron } from 'reactstrap';
import ReactPlayer from 'react-player';

export default function PostShow(){
    const { id } = useParams();
    const [postData, setPostData] = useState([]);

    useEffect(() => {
        axios.get(`/posts/${id}`)
            .then((res) => {
                setPostData(res.data);
            }).catch((err) => {
                console.log(err);
            });
    }, []);
    

    return(
        <div className="container">
            <Jumbotron>
                <h1 className="text-center text-capitalize" style={{margin: "0 0 20px"}}>{postData.title}</h1>
                {
                    postData.type === "video" && (
                        <ReactPlayer
                            style={{margin:"0 auto 20px"}}
                            url={postData.url}
                            playing
                            controls
                            width="auto"
                        />
                        )
                }
                <p className="lead">{postData.description}</p>
            </Jumbotron>
        </div>
    )
}