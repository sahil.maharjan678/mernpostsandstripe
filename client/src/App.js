import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import { Link, Switch, Route, } from 'react-router-dom';
import Post from '../src/components/post/Post';
import PostForm from './components/post/PostForm';
import PostShow from './components/post/PostShow';
import Stripe from './components/stripe/Stripe';
import StripPage2 from './components/stripe/StripeStep2/StripePage2';

function App() {
  return (
   <>
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
              <li className="nav-item">
              <Link to="/" className="nav-link">Home</Link>
              </li>
              <li className="nav-item">
                <Link to="/create_post" className="nav-link">Create Post</Link>
              </li>
            </ul>
        </div>
        <form className="form-inline my-2 my-lg-0">
          <Link to="/stripe" className="btn btn-outline-success my-2 my-sm-0">Stripe(Enter Credit Card Details)</Link>
          <Link to="/stripeStep2" className="btn btn-outline-success my-2 my-sm-0">Checkout</Link>
        </form>
      </div>
    </nav>

    <Switch>
      <Route path="/" exact>
        <Post/>
      </Route>
      <Route path="/create_post" exact>
        <PostForm/>
      </Route>
      <Route path="/post/:id" exact>
        <PostShow/>
      </Route>
      <Route path="/stripe" exact>
        <Stripe/>
      </Route>
      <Route path="/stripeStep2" exact>
        <StripPage2/>
      </Route>
      <Route>
        <Post/>
      </Route>
    </Switch>
   </>
  );
}

export default App;
