const express = require('express');
const app = express();
const mongoose = require('mongoose');
const path = require('path');

require('dotenv/config');

const cors = require('cors');
app.use(cors());

const bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'client/build')));

//Routes
const postsRoute = require('./routes/posts');
app.use('/posts', postsRoute);

const stripeRoute = require('./routes/stripe');
app.use('/stripe', stripeRoute);

//ROOT URL
app.get('/', (req, res) => {
    console.log("home?");
    res.send('We are on home');
});

//connect to DB
mongoose.connect(process.env.DB_CONNECTION,
    { useNewUrlParser: true, useNewUrlParser: true,  useUnifiedTopology: true }, () => {
    console.log("connected to DB");
});

port = process.env.PORT || 3001;
app.listen(port, (req, res) => {
    console.log("listening on port 3001");
});